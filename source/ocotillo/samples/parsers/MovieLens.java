package ocotillo.samples.parsers;

import java.awt.Color;
import java.io.File;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ocotillo.dygraph.DyEdgeAttribute;
import ocotillo.dygraph.DyGraph;
import ocotillo.dygraph.DyNodeAttribute;
import ocotillo.dygraph.Evolution;
import ocotillo.dygraph.FunctionConst;
import ocotillo.geometry.Coordinates;
import ocotillo.geometry.Interval;
import ocotillo.graph.Edge;
import ocotillo.graph.Node;
import ocotillo.graph.StdAttribute;
import ocotillo.samples.parsers.Commons.DyDataSet;
import ocotillo.samples.parsers.Commons.Mode;
import ocotillo.serialization.ParserTools;

public class MovieLens extends PreloadedGraphParser {
	private static class MovieLensDataset{
		Set<String> NodeSet = new HashSet<String>();
		List<Rating> Ratings = new ArrayList<Rating>();
		double startTime = Double.POSITIVE_INFINITY;
		double endTime = Double.NEGATIVE_INFINITY;
	}
	
	private static class Rating{
		String source;
		String target;
		double time;
		double RatingDuration;
	}
	
    public DyDataSet parse(Mode mode) throws URISyntaxException{
        File file = new File("data/MovieLens100k");
        MovieLensDataset dataset = parseMovieLens(file);
        return new DyDataSet(
                parseGraph(file, 2, mode),
                1,
                Interval.newClosed(dataset.startTime, dataset.endTime));
    }
    
    public static DyDataSet parseUndirected(Mode mode) {
        File file = new File("data/MovieLens100k");
        MovieLensDataset dataset = parseMovieLens(file);
        return new DyDataSet(
                parseUndirectedGraph(file, 2, mode),
                1,
                Interval.newClosed(dataset.startTime, dataset.endTime));
    }
	
	public static DyGraph parseGraph(File inputDir, double ratingDuration, Mode mode) {
    	DyGraph graph = new DyGraph();
        DyNodeAttribute<Boolean> presence = graph.nodeAttribute(StdAttribute.dyPresence);
        DyNodeAttribute<String> label = graph.nodeAttribute(StdAttribute.label);
        DyNodeAttribute<Coordinates> position = graph.nodeAttribute(StdAttribute.nodePosition);
        DyNodeAttribute<Color> color = graph.nodeAttribute(StdAttribute.color);
        DyEdgeAttribute<Boolean> edgePresence = graph.edgeAttribute(StdAttribute.dyPresence);
        DyEdgeAttribute<Color> edgeColor = graph.edgeAttribute(StdAttribute.color);
        
        Map<String, Node> nodeMap = new HashMap<>();
        MovieLensDataset dataset = parseMovieLens(inputDir);
        for (String item : dataset.NodeSet) {
            Node node = graph.newNode(item);
            presence.set(node, new Evolution<>(false));
            label.set(node, new Evolution<>(item));
            position.set(node, new Evolution<>(new Coordinates(0, 0)));
            color.set(node, new Evolution<>(new Color(141, 211, 199)));
            nodeMap.put(item, node);
        }
        
        for (Rating rating : dataset.Ratings) {
            Node source = nodeMap.get(rating.source);
            Node target = nodeMap.get(rating.target);
            Edge edge = graph.fromToEdge(source, target);
            if (edge == null) {
                edge = graph.newEdge(source, target);
                edgePresence.set(edge, new Evolution<>(false));
                edgeColor.set(edge, new Evolution<>(Color.BLACK));
            }
            Interval participantPresence = Interval.newRightClosed(
                    rating.time - ratingDuration * rating.RatingDuration * 1,
                    rating.time + ratingDuration * rating.RatingDuration * 2);
            Interval dialogInterval = Interval.newRightClosed(
                    rating.time,
                    rating.time + ratingDuration * rating.RatingDuration);
            presence.get(source).insert(new FunctionConst<>(participantPresence, true));
            presence.get(target).insert(new FunctionConst<>(participantPresence, true));
            edgePresence.get(edge).insert(new FunctionConst<>(dialogInterval, true));
        }
    Commons.scatterNodes(graph, 200);
    Commons.mergeAndColor(graph, dataset.startTime, dataset.endTime + 1, mode, new Color(141, 211, 199), Color.BLACK, 0.001);
    return graph;
	}
	
	public static DyGraph parseUndirectedGraph(File inputDir, double ratingDuration, Mode mode) {
    	DyGraph graph = new DyGraph();
        DyNodeAttribute<Boolean> presence = graph.nodeAttribute(StdAttribute.dyPresence);
        DyNodeAttribute<String> label = graph.nodeAttribute(StdAttribute.label);
        DyNodeAttribute<Coordinates> position = graph.nodeAttribute(StdAttribute.nodePosition);
        DyNodeAttribute<Color> color = graph.nodeAttribute(StdAttribute.color);
        DyEdgeAttribute<Boolean> edgePresence = graph.edgeAttribute(StdAttribute.dyPresence);
        DyEdgeAttribute<Color> edgeColor = graph.edgeAttribute(StdAttribute.color);
        
        Map<String, Node> nodeMap = new HashMap<>();
        MovieLensDataset dataset = parseMovieLens(inputDir);
        for (String item : dataset.NodeSet) {
            Node node = graph.newNode(item);
            presence.set(node, new Evolution<>(false));
            label.set(node, new Evolution<>(item));
            position.set(node, new Evolution<>(new Coordinates(0, 0)));
            color.set(node, new Evolution<>(new Color(141, 211, 199)));
            nodeMap.put(item, node);
        }
        
        for (Rating rating : dataset.Ratings) {
            Node source = nodeMap.get(rating.source);
            Node target = nodeMap.get(rating.target);
            Edge edge1 = graph.fromToEdge(source, target);
            if (edge1 == null) {
                edge1 = graph.newEdge(source, target);
                edgePresence.set(edge1, new Evolution<>(false));
                edgeColor.set(edge1, new Evolution<>(Color.BLACK));
            }
            Edge edge2 = graph.fromToEdge(target, source);
            if(edge2 == null) {
            	edge2 = graph.newEdge(target, source);
            	edgePresence.set(edge2, new Evolution<>(false));
                edgeColor.set(edge2, new Evolution<>(Color.BLACK));
            }
            Interval participantPresence = Interval.newRightClosed(
                    rating.time - ratingDuration * rating.RatingDuration * 1,
                    rating.time + ratingDuration * rating.RatingDuration * 2);
            Interval dialogInterval = Interval.newRightClosed(
                    rating.time,
                    rating.time + ratingDuration * rating.RatingDuration);
            presence.get(source).insert(new FunctionConst<>(participantPresence, true));
            presence.get(target).insert(new FunctionConst<>(participantPresence, true));
            edgePresence.get(edge1).insert(new FunctionConst<>(dialogInterval, true));
            edgePresence.get(edge2).insert(new FunctionConst<>(dialogInterval, true));
        }
    Commons.scatterNodes(graph, 200);
    Commons.mergeAndColor(graph, dataset.startTime, dataset.endTime + 1, mode, new Color(141, 211, 199), Color.BLACK, 0.001);
    return graph;
	}
	
	public static MovieLensDataset parseMovieLens(File inputDir) {
		MovieLensDataset dataset = new MovieLensDataset();
		File dataFile = new File(inputDir + "/u.data");
		List<String> fileLines = ParserTools.readFileLines(dataFile);
		int processedEvent = 0;
		for(String line : fileLines) {
			String[] token = line.split("\t");
			dataset.NodeSet.add(token[0]);
			dataset.NodeSet.add(token[1]);
			
			Rating rating = new Rating();
			rating.source = token[0];
			rating.target = token[1];			
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(Long.parseLong(token[3]) * 1000);
			rating.time = calendar.get(Calendar.YEAR);
			rating.RatingDuration = 0.0833 * (calendar.get(Calendar.MONTH) + 1) + 0.00277 * (calendar.get(Calendar.DAY_OF_MONTH));
			dataset.Ratings.add(rating);
			dataset.startTime = Math.min(rating.time, dataset.startTime);
			dataset.endTime = Math.max(rating.time + rating.RatingDuration + 1, dataset.endTime);
			processedEvent += 1;
			if(processedEvent > 25000) {
				break;
			}
		}
		return dataset;
	}
}
