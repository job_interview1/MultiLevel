package ocotillo;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import ocotillo.dygraph.DyGraph;
import ocotillo.dygraph.extra.SpaceTimeCubeSynchroniser;
import ocotillo.dygraph.extra.SpaceTimeCubeSynchroniser.MirrorLine;
import ocotillo.dygraph.extra.SpaceTimeCubeSynchroniser.StcsBuilder;
import ocotillo.dygraph.layout.fdl.modular.DyModularFdl;
import ocotillo.dygraph.rendering.Animation;
import ocotillo.geometry.Coordinates;
import ocotillo.geometry.Interval;
import ocotillo.graph.Edge;
import ocotillo.graph.Graph;
import ocotillo.graph.Node;
import ocotillo.gui.quickview.DyQuickView;
import ocotillo.gui.quickview.QuickView;

public class Debug {
	
    public static Boolean CheckTrajectoryOverlap(SpaceTimeCubeSynchroniser synchro) {
    	Map<Node, MirrorLine> MirrorLineMap = synchro.mirrorLineMap();
    	ArrayList<Double> Array = new ArrayList<Double>();
    	Set<Double> Set = new LinkedHashSet<Double>();
    	for (MirrorLine MirrorLine : MirrorLineMap.values()) {
    		Coordinates Coordinate = MirrorLine.BendsMidPosition();
    		Array.add(Coordinate.x() + Coordinate.y());
    		Set.add(Coordinate.x() + Coordinate.y());
    	}
    	if(Array.size() == Set.size()) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
	public static Boolean CheckTrajectoryOverlap(DyGraph dygraph, Experiment experiment) {
		/* DyModularFdl algorithm = experiment.getContinuousLayoutAlgorithm(dygraph, null); */
		StcsBuilder builder = new StcsBuilder(dygraph, experiment.dataset.suggestedTimeFactor);
		SpaceTimeCubeSynchroniser synchro = builder.build();
		return CheckTrajectoryOverlap(synchro);
	}
	
	public static void showStaticGraph(Graph graph) {
		QuickView view = new QuickView(graph);
		view.show();
	}
	
	public static void showSpaceTimeCube(DyGraph dygraph, Experiment experiment) {
		/* DyModularFdl algorithm = experiment.getContinuousLayoutAlgorithm(dygraph, null); */
		StcsBuilder builder = new StcsBuilder(dygraph, experiment.dataset.suggestedTimeFactor);
		SpaceTimeCubeSynchroniser synchro = builder.build();
		QuickView.showNewWindow(synchro.mirrorGraph());
	}
	
	public static void showAnimation(DyGraph dygraph, Experiment experiment) {
		DyQuickView view = new DyQuickView(dygraph, experiment.dataset.suggestedInterval.leftBound());
        view.setAnimation(new Animation(experiment.dataset.suggestedInterval, Duration.ofSeconds(10)));
        view.showNewWindow();
	}
	
	public static void showAnimation(DyGraph dygraph, Experiment experiment , double startTime, double endTime, int FrameNumber) {
		Interval interval = Interval.newClosed(startTime, endTime);
		DyQuickView view = new DyQuickView(dygraph, startTime);
        view.setAnimation(new Animation(interval, Duration.ofSeconds(30), FrameNumber));
        view.showNewWindow();
	}
	
	public static void screenshot(Graph graph, String filepath) throws IOException {
		QuickView view = new QuickView(graph);
		BufferedImage img = new BufferedImage(view.getWidth(), view.getHeight(), BufferedImage.TYPE_INT_RGB);
		view.paint(img.getGraphics());
		File outputfile = new File(filepath);
		ImageIO.write(img, "png", outputfile);
	}
}
