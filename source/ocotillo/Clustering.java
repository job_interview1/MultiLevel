package ocotillo;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import ocotillo.dygraph.DyGraph;
import ocotillo.dygraph.DyNodeAttribute;
import ocotillo.dygraph.Evolution;
import ocotillo.dygraph.extra.SpaceTimeCubeSynchroniser;
import ocotillo.dygraph.extra.SpaceTimeCubeSynchroniser.MirrorLine;
import ocotillo.geometry.Coordinates;
import ocotillo.graph.Graph;
import ocotillo.graph.Node;
import ocotillo.graph.NodeAttribute;
import ocotillo.graph.StdAttribute;
import ocotillo.various.ColorCollection;


/* �ˬd Score �p�� */

public class Clustering {
	private static DyGraph dygraph = new DyGraph();
	private static SpaceTimeCubeSynchroniser synchro = null;
	private static double startTime = 0.0;
	private int best_k = 2;
	private int max_k = 10;
	private Map<Node, Coordinates> CoordinatesMap = new HashMap<Node, Coordinates>();
	private Cluster cluster = null;
	
	
	
	public Clustering(DyGraph dygraph, SpaceTimeCubeSynchroniser synchro, double startTime) {
		this.dygraph = dygraph;
		this.CoordinatesMap = ExtractCoordinates(synchro);
		this.startTime = startTime;
		this.synchro = synchro;
		calculateK();
	}
	
	public Cluster getCluster() {
		return this.cluster;
	}
	
	public void calculateK() {
		double max_score = Double.NEGATIVE_INFINITY;
		Cluster best_cluster = null;
		for(int k=2; k<=max_k; k++) {
			Cluster cluster = Kmeans(k);
			double score = cluster.averageSihouetteScore();
			if(score > max_score) {
				this.best_k = k;
				max_score = score;
				best_cluster = cluster;
			}
			System.out.print("K = " + k + ", Average Sihouette Score = " + score + "\n");
		}
		System.out.print("Best K = " + this.best_k + "\n");
		this.cluster = best_cluster;
	}
	
	public Cluster Kmeans(int k){
		Map<Integer, Set<Node>> cluster = new HashMap<Integer, Set<Node>>();
		Map<Integer, Set<Node>> old_cluster = new HashMap<Integer, Set<Node>>();
		ArrayList<Coordinates> centroids = RandomCentroid(k);
		ArrayList<Coordinates> first_centroids = (ArrayList<Coordinates>) centroids.clone();
		int sametime = 0;
		for(int i=0; i<k; i++) {
			cluster.put(i, new HashSet<Node>());
			old_cluster.put(i, new HashSet<Node>());
		}
		for(int i=0; i<300; i++) {
			/* Update Cluster*/
			for(Node node : dygraph.nodes()) {
				int cluster_id = FindNearestCentroid(node, centroids);
				int cluster_id_old = FindCluster(node, cluster);
				if(cluster_id_old != cluster_id) {
					if(cluster.get(cluster_id_old).contains(node)) {
						cluster.get(cluster_id_old).remove(node);
					}
					cluster.get(cluster_id).add(node);
				}else {
					/*
					if(cluster.get(cluster_id_old).contains(node)) {
						cluster.get(cluster_id_old).remove(node);
					}
					*/
					cluster.get(cluster_id).add(node);
				}
			}
			if(SameCluster(old_cluster, cluster)) {
				sametime += 1;
			}else {
				sametime = 0;
			}
			
			if(sametime == 10) {
				Cluster result = new Cluster(cluster, first_centroids, centroids);
				return result;
			}
			/* Update Centroid */
			old_cluster = copyCluster(cluster);
			UpdateCentroid(centroids, cluster);
			/* checkStable */
		}
		Cluster result = new Cluster(cluster, first_centroids, centroids);
		return result;
	}
	
	public Cluster KmeansWithCentroidsAtTime(Graph graph){
		Map<Node, Coordinates> staticCoordinatesMap = ExtractStaticCoordinates(graph);
		ArrayList<Coordinates> centroids = this.cluster.centroids;
		Map<Integer, Set<Node>> cluster = new HashMap<Integer, Set<Node>>();
		Map<Integer, Set<Node>> old_cluster = new HashMap<Integer, Set<Node>>();
		int sametime = 0;
		for(int i=0; i<centroids.size(); i++) {
			cluster.put(i, new HashSet<Node>());
			old_cluster.put(i, new HashSet<Node>());
		}
		for(int i=0; i<300; i++) {
			/* Update Cluster*/
			for(Node node : graph.nodes()) {
				int cluster_id = FindNearestCentroid(node, centroids, staticCoordinatesMap);
				int cluster_id_old = FindCluster(node, cluster);
				if(cluster_id_old != cluster_id) {
					if(cluster.get(cluster_id_old).contains(node)) {
						cluster.get(cluster_id_old).remove(node);
					}
					cluster.get(cluster_id).add(node);
				}else {
					/* if(cluster.get(cluster_id_old).contains(node)) {
						cluster.get(cluster_id_old).remove(node);
					} */
					cluster.get(cluster_id).add(node);
				}
			}
			
			if(SameCluster(old_cluster, cluster)) {
				sametime += 1;
			}else {
				sametime = 0;
			}
			
			if(sametime == 10) {
				Cluster result = new Cluster(cluster, centroids, centroids);
				return result;
			}
			/* Update Centroid */
			old_cluster = copyCluster(cluster);
			/* UpdateCentroid(centroids, cluster, staticCoordinatesMap); */
			UpdateCentroid(centroids, this.getCluster().NodeCluster);
			/* checkStable */
		}
		Cluster result = new Cluster(cluster, centroids, centroids);
		return result;
	}

	
	/* Copy Cluster1 -> Cluster2 */
	private static Map<Integer, Set<Node>> copyCluster(Map<Integer, Set<Node>> map) {
		Map<Integer, Set<Node>> copymap = new HashMap<Integer, Set<Node>>();
		for(Entry<Integer, Set<Node>> entry : map.entrySet()) {
			copymap.put(entry.getKey(), new HashSet<>(entry.getValue()));
		}
		return copymap;
	}
	
	private static Boolean SameCluster(Map<Integer, Set<Node>> old_cluster, Map<Integer, Set<Node>> cluster) {
		Boolean result = false;
		for(int i=0; i<cluster.size(); i++) {
			if(!old_cluster.get(i).equals(cluster.get(i))) {
				return result;
			}
		}
		result = true;
		return result;		
	}
	
	public static int FindCluster(Node node, Map<Integer, Set<Node>> cluster) {
		int cluster_id = 0;
		for(Entry<Integer, Set<Node>> entry : cluster.entrySet()) {
			if(entry.getValue().contains(node)) {
				cluster_id = entry.getKey();
				break;
			}
		}
		return cluster_id;
		
	}

	private void UpdateCentroid(ArrayList<Coordinates> centroids, Map<Integer, Set<Node>> cluster) {
		for(int i=0; i<cluster.size(); i++) {
			centroids.set(i, AveragePosition(cluster.get(i)));
		}
	}
	

	
	private Coordinates AveragePosition(Set<Node> cluster) {
		double X = 0.0;
		double Y = 0.0;
		for(Node node : cluster) {
			X += CoordinatesMap.get(node).x();
			Y += CoordinatesMap.get(node).y();
		}
		X = X / cluster.size();
		Y = Y / cluster.size();
		return new Coordinates(X, Y);
	}
	
	
	private int FindNearestCentroid(Node node, ArrayList<Coordinates> centroids) {
		double distance = Double.POSITIVE_INFINITY;
		int cluster_id = 0;
		for(Coordinates centroid : centroids) {
			if(NodeDistance(CoordinatesMap.get(node), centroid) < distance) {
				distance = NodeDistance(CoordinatesMap.get(node), centroid);
				cluster_id = centroids.indexOf(centroid);
			}
		}
		return cluster_id;
	}
	
	private int FindNearestCentroid(Node node, ArrayList<Coordinates> centroids, Map<Node, Coordinates> StaticCoordinatesMap) {
		double distance = Double.POSITIVE_INFINITY;
		int cluster_id = 0;
		for(Coordinates centroid : centroids) {
			if(NodeDistance(StaticCoordinatesMap.get(node), centroid) < distance) {
				distance = NodeDistance(StaticCoordinatesMap.get(node), centroid);
				cluster_id = centroids.indexOf(centroid);
			}
		}
		return cluster_id;
	}
	
	private static double NodeDistance(Coordinates A, Coordinates B) {
		return Math.sqrt(Math.pow(A.x() - B.x(), 2) + Math.pow(A.y() - B.y(), 2));
	}
	
	
	private ArrayList<Coordinates> RandomCentroid(int k) {
		ArrayList<Coordinates> result = new ArrayList<Coordinates>();
		ArrayList<Node> nodeList = new ArrayList<Node>(dygraph.nodes());
		Node centroid = null;
		for(int i=0; i<k; i++) {
			if(i==0) {
				int random = 0 + ( (int) Math.random() * (nodeList.size() - 0));
				centroid = nodeList.get(random);
				nodeList.remove(centroid);
			}
			else {
				Map<Node, Double> ProbabilityMap = calculateProbability(centroid, nodeList);
				centroid = randomSelect(ProbabilityMap);
				nodeList.remove(centroid);
			}
			result.add(CoordinatesMap.get(centroid));
		}
		
		return result;
	}
	
	private static Node randomSelect(Map<Node, Double> ProbabilityMap) {
		double p = Math.random();
		double cumulativeProbability = 0.0;
		Node result = null;
		for(Node N : ProbabilityMap.keySet()) {
			cumulativeProbability += ProbabilityMap.get(N);
			if(p <= cumulativeProbability) {
				result = N;
				break;
			}
		}
		return result;
	}
	
	private Map<Node, Double> calculateProbability(Node node, ArrayList<Node> nodeList) {
		Map<Node, Double> ProbabilityMap = new HashMap<Node, Double>();
		Map<Node, Double> DistanceMap = new HashMap<Node, Double>();
		Double DistanceSum = 0.0;
		/* Build DistanceMap */
		for(Node N : nodeList) {
			Double Distance = Math.pow(NodeDistance(CoordinatesMap.get(node), CoordinatesMap.get(N)), 2);
			DistanceMap.put(N, Distance);
			DistanceSum += Distance;
		}
		/* Build ProbabilityMap */
		
		for(Node N : nodeList) {
			Double Probability = DistanceMap.get(N) / DistanceSum;
			ProbabilityMap.put(N, Probability);
		}
		return ProbabilityMap;
	}
	
	private Map<Node, Double> calculateProbability(Node node, ArrayList<Node> nodeList, Map<Node, Coordinates> staticCoordinatesMap) {
		Map<Node, Double> ProbabilityMap = new HashMap<Node, Double>();
		Map<Node, Double> DistanceMap = new HashMap<Node, Double>();
		Double DistanceSum = 0.0;
		/* Build DistanceMap */
		for(Node N : nodeList) {
			Double Distance = Math.pow(NodeDistance(staticCoordinatesMap.get(node), staticCoordinatesMap.get(N)), 2);
			DistanceMap.put(N, Distance);
			DistanceSum += Distance;
		}
		/* Build ProbabilityMap */
		
		for(Node N : nodeList) {
			Double Probability = DistanceMap.get(N) / DistanceSum;
			ProbabilityMap.put(N, Probability);
		}
		return ProbabilityMap;
	}
	
	
	public void ColorNode() {
		DyNodeAttribute<Color> nodeColors = dygraph.nodeAttribute(StdAttribute.color);
        for (Node node : dygraph.nodes()) {
        	int clusterid = FindCluster(node, this.cluster.NodeCluster);
        	nodeColors.set(node, new Evolution<>(ColorCollection.cbQualitativePastel.get(clusterid)));
        }
	}
	
	public void showcluster(Graph graph, int clusterid) {
		NodeAttribute<Color> nodeColors = graph.nodeAttribute(StdAttribute.color);
		NodeAttribute<Object> nodePosition = graph.nodeAttribute(StdAttribute.nodePosition);
		NodeAttribute<Object> nodeLabel = graph.nodeAttribute(StdAttribute.label);
		Graph clusterGraph = new Graph();
		NodeAttribute<Color> clusternodeColors = clusterGraph.nodeAttribute(StdAttribute.color);
		NodeAttribute<Object> clusternodePosition = clusterGraph.nodeAttribute(StdAttribute.nodePosition);
		NodeAttribute<Object> clusternodeLabel = clusterGraph.nodeAttribute(StdAttribute.label);
		for (Node node : dygraph.nodes()) {
			if(clusterid == FindCluster(node, this.cluster.NodeCluster)) {
				clusterGraph.add(node);
				clusternodeColors.set(node, ColorCollection.cbQualitativePastel.get(clusterid));
				clusternodePosition.set(node, nodePosition.get(node));
				clusternodeLabel.set(node, nodeLabel.get(node));
			}
		}
		Debug.showStaticGraph(clusterGraph);
	}
	
	
	private static Map<Node, Coordinates> ExtractCoordinates(SpaceTimeCubeSynchroniser synchro){
		Map<Node, Coordinates> result = new HashMap<Node, Coordinates>();
		Map<Node, MirrorLine> MirrorLineMap = synchro.mirrorLineMap();
		for(Node node : dygraph.nodes()) {
			/* Coordinates coor = (Coordinates) MirrorLineMap.get(node).BendsMidPosition(); */
			/* Coordinates coor = (Coordinates) MirrorLineMap.get(node).MirrorLineMid(); */
			Coordinates coor = (Coordinates) MirrorLineMap.get(node).bendsAndExtremities().get(0);
			result.put(node, coor);
		}
		return result;
	}
	
	private static Map<Node, Coordinates> ExtractStaticCoordinates(Graph graph){
		Map<Node, Coordinates> result = new HashMap<Node, Coordinates>();
		NodeAttribute<Coordinates> position = graph.nodeAttribute(StdAttribute.nodePosition);
		for(Node node : graph.nodes()) {
			result.put(node, position.get(node));
		}
		return result;
	}
	
	public class Cluster { 
		public Map<Integer, Set<Node>> NodeCluster;
		private ArrayList<Coordinates> FirstCentroids;
		private ArrayList<Coordinates> centroids;
		
		public Cluster(Map<Integer, Set<Node>> NodeCluster, ArrayList<Coordinates> FirstCentroids, ArrayList<Coordinates> centroids) {
			this.NodeCluster = NodeCluster;
			this.FirstCentroids = FirstCentroids;
			this.centroids = centroids;
		}
		
		public ArrayList<Coordinates> getCentroids(){
			return this.centroids;
		}
		
		public double averageIntraClusterDistance() {
			double result = 0.0;
			for(int i=0; i<NodeCluster.size(); i++) {
				double intraClusterDistance = 0.0;
				for(Node node : NodeCluster.get(i)) {
					intraClusterDistance += NodeDistance(CoordinatesMap.get(node), centroids.get(i));
				}
				intraClusterDistance /= NodeCluster.get(i).size();
				result += intraClusterDistance;
			}
			result /= NodeCluster.size();
			return result;
		}
		
		
		public double interClusterDistance(Node node) {
			double result = Double.POSITIVE_INFINITY;
			int cluster_id = FindCluster(node, NodeCluster);
			for(int i=0; i<NodeCluster.size(); i++) {

				if(i != cluster_id) {
					double sum = 0.0;
					for(Node OtherClusterNode : NodeCluster.get(i)) {
						sum += NodeDistance(CoordinatesMap.get(node), CoordinatesMap.get(OtherClusterNode));
					}
					if(sum < result) {
						result = sum / NodeCluster.get(i).size();
					}
				}
			}
			return result;
		}
		
		public double intraClusterDistance(Node node) {
			double result = 0.0;
			int cluster_id = FindCluster(node, NodeCluster);
			for(Node SameClusterNode : NodeCluster.get(cluster_id)) {
				result += NodeDistance(CoordinatesMap.get(node), CoordinatesMap.get(SameClusterNode));
			}
			return result / (NodeCluster.get(cluster_id).size() - 1);
		}
		
		public double averageSihouetteScore() {
			double result = 0.0;
			for(Node node : dygraph.nodes()){
				double a = intraClusterDistance(node);
				double b = interClusterDistance(node);
				result = result + ((b-a) / Math.max(a, b));
			}
			return result / dygraph.nodes().size();
		}
	
	
	}
}
