package ocotillo;


import java.util.List;

import ocotillo.Experiment;
import ocotillo.Experiment.InfoVis;
import ocotillo.dygraph.DyGraph;
import ocotillo.dygraph.layout.fdl.modular.DyModularFdl;
import ocotillo.graph.Graph;
import ocotillo.graph.StdAttribute;
import ocotillo.graph.layout.fdl.modular.ModularPostProcessing;
import ocotillo.graph.layout.fdl.modular.ModularStatistics;
import ocotillo.graph.multilevel.layout.MultiLevelDynNoSlice;
import ocotillo.multilevel.coarsening.SolarMerger;
import ocotillo.multilevel.flattener.DyGraphFlattener;
import ocotillo.multilevel.options.MultiLevelDrawingOption;
import ocotillo.multilevel.placement.WeightedBarycenterPlacementStrategy;
import ocotillo.multilevel.placement.WeightedBarycenterPlacementStrategy.SolarMergerPlacementStrategy;
import ocotillo.run.Run;
import ocotillo.gui.quickview.DyQuickView;
import ocotillo.dygraph.rendering.Animation;
import java.time.Duration;

public class Testing {
	public static void main(String[] args) throws Exception {
		Experiment experiment = new Experiment.Pride();
		DyGraph ContGraph = experiment.dataset.dygraph;
		DyGraph DiscGraph = experiment.discretise();
		List<Double> snapTimes = experiment.readSnapTimes(DiscGraph);
		MultiLevelDynNoSlice multiDyn = 
				new MultiLevelDynNoSlice(ContGraph, 1.0, Run.defaultDelta)
				.setCoarsener(new SolarMerger()) //WalshawIndependentSet
				.setPlacementStrategy(new SolarMergerPlacementStrategy())
				.setFlattener(new DyGraphFlattener.StaticSumPresenceFlattener())
				.defaultLayoutParameters()
				.addLayerPostProcessingDrawingOption(new MultiLevelDrawingOption.FlexibleTimeTrajectoriesPostProcessing(2))
				.addOption(MultiLevelDynNoSlice.LOG_OPTION, true).build();
		ContGraph = multiDyn.runMultiLevelLayout();
		/* DyModularFdl contAlgorithm = experiment.getContinuousLayoutAlgorithm(ContGraph, null);
		contAlgorithm.iterate(100);*/
		DyQuickView view = new DyQuickView(ContGraph, experiment.dataset.suggestedInterval.leftBound(), "Pride");
		view.setAnimation(new Animation(experiment.dataset.suggestedInterval, java.time.Duration.ofSeconds(10)));
		view.showNewWindow();
		}
	}
